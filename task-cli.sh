FOLDER="irsyad at `Date`"
mkdir "$FOLDER"
cd "$FOLDER"
mkdir about_me
cd about_me
mkdir personal
mkdir professional
cd personal
echo "https://www.facebook.com/irsyad.fauzann/" > facebook.txt
cd ..
cd professional
echo "https://www.facebook.com/irsyad.fauzann/" > linkedin.txt
cd ..
cd ..
mkdir my_friends
cd my_friends
MYAPI=$(curl --location --request GET 'https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt')
echo $MYAPI > my_friends.txt
cd ..
mkdir my_system_info
cd my_system_info
USERNAME=$(id -un)
echo "My username:" $USERNAME > about_this_laptop.txt
MYHOST=$(uname -a)
echo "With host: $MYHOST" >> about_this_laptop.txt
MYPING=$(ping -c 3 google.com)
echo "Connection to google: \n$MYPING" > internet_connection.txt


